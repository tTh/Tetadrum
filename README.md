# TetaDrum

**Boite à rhytmes expérimentale et modulaire**

En fait, j'ai plein de docs sur papier : des dessins, des schémas,
des organigrammes. Hélas, ma flemme modère fortement mon envie
de mettre tout ça en ligne ;(

## Principe général

Une architecture en trois grandes zones :

* Un bus central alim/controle/son
* Plusieurs modules sons (Eg: ne555 + filtres résonnants)
* Un séquenceur avec une IHM sur un MSX en Basic

## Détails techniques


## Logiciels

Tout reste à écrire, nous attendons juste que les branleurs du hard
publient les spécifications.


